package;

import tink.testrunner.Assertion;
import tink.unit.Assert.assert;
import tink.unit.AssertionBuffer;
import libgoa.Itzul;

using tink.CoreApi;

class ItzulEmbedTest  {

    public function new() {}
    
    @:setup
    public function readPoFiles() {
       Itzul.embed();
       return Noise;
    }
    
    public function loadedCount() {
        return assert(Itzul.getInstance().locales.length == 4);
    }
    
    @:variant('en_GB', "Sadko's shadow")
    @:variant('es_ES', 'La sombra de Sadko')
    @:variant('eu_ES', 'Sadkoren itzala')
    @:variant('fr_FR', "L'ombre de Sadko")
    public function getId(loc:String, str:String) {
        
        var buff = new AssertionBuffer();
        
        Itzul.getInstance().setLocale(loc);
        buff.assert(Itzul.sadkoren_itzala.translate() == str);
        
        Itzul.getInstance().setLocale('false_locale');
        buff.assert(Itzul.sadkoren_itzala.translate() == 'Sadkoren itzala');
        
        return buff.done();
    }
}