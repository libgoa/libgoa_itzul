package;

import tink.testrunner.Assertion;
import tink.unit.Assert.assert;
import tink.unit.AssertionBuffer;
import libgoa.Itzul;
import libgoa.ItzulId;

using tink.CoreApi;

class ItzulInstanceTest  {

    public function new() {}
    
    @:setup
    public function readPoFiles() {
        
        Itzul.getInstance().strings = new Map();
        Itzul.getInstance().locales = ['en_GB', 'es_ES', 'eu_ES', 'fr_FR'];
        var locStrings;
        for (l in Itzul.getInstance().locales){
            locStrings = libgoa.builder.itzul.TranslationProcessor.processPoContent(l, haxe.Resource.getString('$l.po'));
            if (locStrings != null)
                Itzul.getInstance().strings.set(l, locStrings);
        }
        return Noise;
    }
    
    public function loadedCount() {
        return assert(Itzul.getInstance().locales.length == 4);
    }
    
    @:variant('en_GB')
    @:variant('es_ES')
    @:variant('eu_ES')
    @:variant('fr_FR')
    public function setLocale(loc:String) {
        Itzul.getInstance().setLocale(loc);
        return assert(Itzul.getInstance().currentLocale == loc);
    }
    
    @:variant('en_GB', "Sadko's shadow")
    @:variant('es_ES', 'La sombra de Sadko')
    @:variant('eu_ES', 'Sadkoren itzala')
    @:variant('fr_FR', "L'ombre de Sadko")
    public function getId(loc:String, str:String) {
        
        var buff = new AssertionBuffer();
        
        var id:ItzulId = 'Sadkoren itzala';
        
        Itzul.getInstance().setLocale(loc);
        buff.assert(id.translate() == str);
        
        Itzul.getInstance().setLocale('false_locale');
        buff.assert(id.translate(loc) == str);
        buff.assert(new ItzulId('notExistingId').translate(loc) == 'notExistingId');
        buff.assert(new ItzulId(null).translate(loc) == '__notfound__');
        
        buff.assert(id.translate('notExistingLoc') == id);
        
        return buff.done();
    }
}