package libgoa;

#if !macro
@:build(libgoa.builder.itzul.LocalesBuilder.build())
#end
class Itzul {
    
    static var inst:Itzul;
    static public function getInstance() {
        if (inst == null)
            inst = new Itzul();
        return inst;
    }
    
    // Map<locale_code, Map<localizableId, localizedText>>
    public var strings:Map<String, Map<String, String>>;
    public var currentLocale:String;
    public var locales:Array<String>;
    
    public inline function new() {}
    
    public inline function setLocale(_newLoc:String) currentLocale = _newLoc;

    static macro public function embed() {
        return macro @:privateAccess {
                libgoa.Itzul.getInstance().strings = libgoa.builder.itzul.EmbedLocales.embed();
                if (libgoa.Itzul.getInstance().strings != null) {
                    var keys = libgoa.Itzul.getInstance().strings.keys();
                    libgoa.Itzul.getInstance().setLocale((keys.hasNext()) ? keys.next() : '');
                }
            }
    }

    public function translate(id:ItzulId, defLoc:String = null) : String {

        var localizedText = translateToLocale(id, currentLocale);
        if (localizedText == id && defLoc != null)
            localizedText = translateToLocale(id, defLoc);
        
        return localizedText;
    }

    @:noCompletion
    public function translateToLocale(id:ItzulId, loc:String) : String {
        if (strings == null || id == null || loc == null)
            return (id == null) ? '__notfound__' : id;
        
        var _retText = id;
        var _locMap = strings.get(loc);
        if (_locMap != null) {
            _retText = _locMap.get(_retText);
        }
        return (_retText != null) ? _retText : id;
    }
}