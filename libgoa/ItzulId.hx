package libgoa;


abstract ItzulId(String) {
    
    inline public function new(id) this = id;
    
    @:to inline public function toString():String return this;
    
    inline public function translate(locale = null)
        return Itzul.getInstance().translate(new ItzulId(this), locale);
    
    @:from static public inline function fromString(s:String):ItzulId
        return new ItzulId(s);
}