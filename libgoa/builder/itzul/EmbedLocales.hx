package libgoa.builder.itzul;

import haxe.macro.Context;

class EmbedLocales {
    static public macro function embed() {
        var localesDir = Context.definedValue("localesPath");
        if( localesDir == null ) localesDir = "locales/";
		var pos = Context.currentPos();
		var dir = try Context.resolvePath(localesDir) 
                catch( e : Dynamic ) { 
                    Context.warning("Localizations directory not found in classpath '" + localesDir + "' (use -D localesPath=DIR)", pos);
                    "__invalid";
                }
        if (dir == '__invalid')
            return macro null;
        var localesPath = '';
		localesPath = sys.FileSystem.fullPath(dir);
		if( !sys.FileSystem.exists(localesPath) || !sys.FileSystem.isDirectory(localesPath) )
			Context.warning("Localizations directory does not exists '" + localesPath + "'", pos);
        if (!sys.FileSystem.isDirectory(localesPath))
            Context.error('$localesPath must be a directory.', Context.currentPos());
        
        if (!StringTools.endsWith(localesPath, '/')) localesPath += '/';
        
        var createdFields;
        var locName, locStrings, locId, locMsg, escapeChar, content, locales = [];
        var escapeEreg = ~/\\(n|t)/g;
        try {
            for( poFile in sys.FileSystem.readDirectory(localesPath) ) {
                createdFields = new Map();
                locName = StringTools.replace(poFile, '.po', '');
                locStrings = [];
                content = sys.io.File.getContent(localesPath+poFile);
                
                while (AdErr.PO_MSG.match(content)) {
                    locId = AdErr.PO_MSG.matched(1);
                    locMsg = AdErr.PO_MSG.matched(2);
                    if (escapeEreg.match(locMsg)) {
                        escapeChar = switch escapeEreg.matched(1) {
                                            case 'n': '\n';
                                            case 't': '\t';
                                            case _: '';
                                        }
                        locMsg = escapeEreg.replace(locMsg, escapeChar);
                    }
                    if (!createdFields.exists(locId)) {
                        locStrings.push(macro $v{locId} => $v{locMsg});
                        createdFields.set(locId, true);
                    }
                    content = AdErr.PO_MSG.matchedRight();
                }

                locales.push(macro $v{locName} => $a{locStrings});
            }

        } catch (e:Any) {
            trace('error reading po files from $localesPath: $e');
        }
        
        return macro $a{locales};
    }
}