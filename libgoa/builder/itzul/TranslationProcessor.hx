package libgoa.builder.itzul;

class TranslationProcessor {
    
    static public function processPoContent(locName:String, content:String) {
        var locId, locMsg, escapeChar;
        var escapeEreg = ~/\\(n|t)/g;
            
        var locStrings = new Map();
        
        while (AdErr.PO_MSG.match(content)) {
            locId = AdErr.PO_MSG.matched(1);
            locMsg = AdErr.PO_MSG.matched(2);
            if (escapeEreg.match(locMsg)) {
                escapeChar = switch escapeEreg.matched(1) {
                                    case 'n': '\n';
                                    case 't': '\t';
                                    case _: '';
                                }
                locMsg = escapeEreg.replace(locMsg, escapeChar);
            }
            locStrings.set(locId, locMsg);
            content = AdErr.PO_MSG.matchedRight();
        }
        
        return locStrings;
    }
    
    static public function processPoFiles(localesRelDir:String) {
        try {
    		var exePath = null;
            var froot = exePath == null ? localesRelDir : sys.FileSystem.fullPath(exePath.join("/") + "/" + localesRelDir);
            if( froot == null || !sys.FileSystem.exists(froot) || !sys.FileSystem.isDirectory(froot) ) {
                froot = sys.FileSystem.fullPath(localesRelDir);
                if( froot == null || !sys.FileSystem.exists(froot) || !sys.FileSystem.isDirectory(froot) )
                    throw "Could not find dir " + localesRelDir;
            }
            var localesDir = froot.split("\\").join("/");
            if (!StringTools.endsWith(localesDir, '/')) localesDir += '/';
            
            Itzul.getInstance().strings = new Map();
            Itzul.getInstance().locales = [];
            var locName = '', locStrings;
            for( poFile in sys.FileSystem.readDirectory(localesDir) ) {
                locName = StringTools.replace(poFile, '.po', '');
                Itzul.getInstance().locales.push(locName);
                locStrings = processPoContent(locName, sys.io.File.getContent(localesDir + poFile));
                if (locStrings != null)
                    Itzul.getInstance().strings.set(locName, locStrings);
            }
            Itzul.getInstance().setLocale(locName);

        } catch (e:Any) {
            trace('error reading po files from $localesRelDir: $e');
        }
        
        return Itzul.getInstance().strings;
    }
}